﻿using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Module4");
        }

        public int Task_1_A(int[] array)
        {
            if ((array == null) || (array.Length == 0))
                throw new ArgumentNullException("array");
            int max = array.Max();           
            return max;
        }

        public int Task_1_B(int[] array)
        {
            if ((array == null) || (array.Length == 0))
                throw new ArgumentNullException("array");
            int min = array.Min();           
            return min;
        }

        public int Task_1_C(int[] array)
        {
            if ((array == null) || (array.Length == 0))
                throw new ArgumentNullException("array");
            int sum = array.Sum();            
            return sum;
        }

        public int Task_1_D(int[] array)
        {
            if ((array == null) || (array.Length == 0))
                throw new ArgumentNullException("array");
            int res = Task_1_A(array) - Task_1_B(array);
            return res;
        }

        public void Task_1_E(int[] array)
        {
            if ((array == null) || (array.Length == 0))
                throw new ArgumentNullException("array");
            int max = Task_1_A(array);
            int min = Task_1_B(array);
            for (int i = 0; i < array.Length; ++i)
            {
                if (i % 2 == 0)
                    array[i] += max;
                else
                    array[i] -= min;
            }
        }
        public int Task_2(int a, int b, int c)
        {
            int res = a + b + c;
            return res;
        }

        public int Task_2(int a, int b)
        {
            int res = a + b;
            return res;
        }

        public double Task_2(double a, double b, double c)
        {
            double res = a + b + c;
            return res;
        }

        public string Task_2(string a, string b)
        {
            string res = a + b;
            return res;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            if ((a == null) || (a.Length == 0))
                throw new ArgumentNullException("a");
            if ((b == null) || (b.Length == 0))
                throw new ArgumentNullException("b");
            int max = Math.Max(a.Length, b.Length);
            int[] res = new int[max];
            for (int i = 0; i < max; i++)
            {
                if (i < a.Length)
                    res[i] += a[i];
                if (i < b.Length)
                    res[i] += b[i];
            }
            return res;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
            {
                throw new ArgumentNullException("radius");
            }
            length = 2 * Math.PI * radius;
            square = Math.PI * Math.Pow(radius, 2);            
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            if ((array == null)||(array.Length==0))
            {
                throw new ArgumentNullException("array");
            }
            maxItem = array.Max();
            minItem = array.Min();
            sumOfItems = array.Sum();
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;
            return numbers;
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentNullException("radius");
            }
            double length = 2 * Math.PI * radius;
            double square = Math.PI * Math.Pow(radius, 2);
            return (length, square);
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            if ((array == null) || (array.Length == 0))
            {
                throw new ArgumentNullException("array");
            }
            int maxItem = array.Max();
            int minItem = array.Min();
            int sumOfItems = array.Sum();
            return (minItem, maxItem, sumOfItems);
        }

        public void Task_5(int[] array)
        {
            if ((array == null) || (array.Length == 0))
            {
                throw new ArgumentNullException("array");
            }
            for (int i = 0; i < array.Length; ++i)
            {
               array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if ((array == null) || (array.Length == 0) || (array.Length == 1))
            {
                throw new ArgumentNullException("array");
            }
            for (int i = 1; i < array.Length; ++i)
            {
                if (CheckNeedSort(array[i], array[i - 1], direction))
                {
                    int tmp = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = tmp;
                    if (i != 1)
                        i -= 2;
                }
            }          
        }

        private bool CheckNeedSort(int a, int b, SortDirection direction)
        {
            if (direction == SortDirection.Ascending)
            {
                return (a < b);
            }
            else
            {
                return (a > b);
            }
        }

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            if (func == null)
                throw new ArgumentNullException("func");
            double x = (x1 + x2) / 2.0;
            if (func(x) == 0)
                result = x;
            else
            {
                if ((func(x1) * func(x)) < 0)
                    x2 = x;
                else if ((func(x2) * func(x)) < 0)
                    x1 = x;
                if (Math.Abs(x1 - x2) > 2 * e)
                    result = Task_7(func, x1, x2, e, result);
                else result = x;
            }
            return result;
        }
    }
}
